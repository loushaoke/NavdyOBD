// (c) 2013-2015 Don Coleman
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/* global mainPage, deviceList, refreshButton, statusDiv */
/* global detailPage, resultDiv, messageInput, sendButton, disconnectButton */
/* global cordova, bluetoothSerial  */
/* jshint browser: true , devel: true*/
'use strict';

var toArrayBuffer = function(data) {
    // convert to ArrayBuffer
    if (typeof data === 'string') {
        var ret = new Uint8Array(data.length);
        for (var i = 0; i < data.length; i++) {
            ret[i] = data.charCodeAt(i);
        }
        data = ret.buffer
    } else if (data instanceof Array) {
        // assuming array of interger
        data = new Uint8Array(data).buffer;
    } else if (data instanceof Uint8Array) {
        data = data.buffer;
    }

    return data;
};

var arrayToString = function (data) {
    var dec = new TextDecoder("ascii")
    return dec.decode(data)
}

var app = {
    initialize: function() {
        this.bindEvents();
        this.showMainPage();
    },
    bindEvents: function() {

        var TOUCH_START = 'touchstart';
        if (window.navigator.msPointerEnabled) { // windows phone
            TOUCH_START = 'MSPointerDown';
        }
        document.addEventListener('deviceready', this.onDeviceReady, false);
        refreshButton.addEventListener(TOUCH_START, this.refreshDeviceList, false);
        sendButton.addEventListener(TOUCH_START, this.sendData, false);
        disconnectButton.addEventListener(TOUCH_START, this.disconnect, false);
        deviceList.addEventListener('touchstart', this.connect, false);
    },

    tcpServerSocketId: null,
    tcpClientSocketId: null,
    logfile: null,

    startServer: function() {
        chrome.sockets.tcpServer.create({}, function(createInfo) {
            app.tcpListenAndAccept(createInfo.socketId);
        });
    },
    tcpListenAndAccept: function (socketId) {
        var port = 35000;
        chrome.sockets.tcpServer.listen(socketId,
            "localhost", port, function(resultCode) {
            app.tcpOnListenCallback(socketId, resultCode)
        });
    },
    tcpOnListenCallback: function (socketId, resultCode) {
        if (resultCode < 0) {
            console.log("Error listening:" +
            chrome.runtime.lastError.message);
            return;
        }
        app.tcpServerSocketId = socketId;
        chrome.sockets.tcpServer.onAccept.addListener(app.tcpOnAccept)
    },
    tcpOnAccept: function (info) {
        if (info.socketId != app.tcpServerSocketId)
            return;
        app.tcpClientSocketId = info.clientSocketId;
        
        // A new TCP connection has been established.
        
        // chrome.sockets.tcp.send(info.clientSocketId, data,
        //     function(resultCode) {
        //     console.log("Data sent to new TCP client connection.")
        // });

        chrome.sockets.tcp.secure(app.tcpClientSocketId, function(result) {
            console.log("secure:");
            console.log(result)

            // Start receiving data.
            chrome.sockets.tcp.onReceive.addListener(function(recvInfo) {
                if (recvInfo.socketId != info.clientSocketId)
                return;

                app.sendData(recvInfo.data)
            });
            // chrome.sockets.tcp.setKeepAlive(app.tcpServerSocketId, true);
            chrome.sockets.tcp.setPaused(app.tcpServerSocketId, false);
            chrome.sockets.tcp.setPaused(app.tcpClientSocketId, false);
        })
},
    
    onDeviceReady: function() {
        app.refreshDeviceList();
        app.startServer();
    },
    refreshDeviceList: function() {
        bluetoothSerial.list(app.onDeviceList, app.onError);
    },
    onDeviceList: function(devices) {
        var option;

        // remove existing devices
        deviceList.innerHTML = "";
        app.setStatus("");

        devices.forEach(function(device) {

            var listItem = document.createElement('li'),
                html = '<b>' + device.name + '</b><br/>' + device.id;

            listItem.innerHTML = html;

            if (cordova.platformId === 'windowsphone') {
              // This is a temporary hack until I get the list tap working
              var button = document.createElement('button');
              button.innerHTML = "Connect";
              button.addEventListener('click', app.connect, false);
              button.dataset = {};
              button.dataset.deviceId = device.id;
              listItem.appendChild(button);
            } else {
              listItem.dataset.deviceId = device.id;
            }
            deviceList.appendChild(listItem);
        });

        if (devices.length === 0) {

            option = document.createElement('option');
            option.innerHTML = "No Bluetooth Devices";
            deviceList.appendChild(option);

            if (cordova.platformId === "ios") { // BLE
                app.setStatus("No Bluetooth Peripherals Discovered.");
            } else { // Android or Windows Phone
                app.setStatus("Please Pair a Bluetooth Device.");
            }

        } else {
            app.setStatus("Found " + devices.length + " device" + (devices.length === 1 ? "." : "s."));
        }

    },
    connect: function(e) {
        var onConnect = function() {
            cordova.plugins.backgroundMode.enable();    
            
            // subscribe for incoming data
            bluetoothSerial.subscribeRawData(app.onData, app.onError);

            resultDiv.innerHTML = "";
            app.setStatus("Connected");
            app.showDetailPage();

            
            var logpath = null
            if (cordova.file.externalRootDirectory) {
                logpath = cordova.file.externalRootDirectory
            } else if (cordova.file.syncedDataDirectory){
                logpath = cordova.file.syncedDataDirectory
            }
            if (logpath !== null) {
                window.resolveLocalFileSystemURL(logpath, function(dir) {
                    var now = new Date();
                    var ts = now.toISOString().replace(/-/g,'').replace(/:/g,'').replace('T','_').split('.')[0]
                    dir.getDirectory('NavdyOBD', { create: true }, function (dirEntry) {
                        dirEntry.getFile("NavdyOBD_"+ts+".txt", {create:true}, function(file) {
                        app.logfile = file
                    });
                });
            });
            }
        };

        var deviceId = e.target.dataset.deviceId;
        if (!deviceId) { // try the parent
            deviceId = e.target.parentNode.dataset.deviceId;
        }

        bluetoothSerial.connect(deviceId, onConnect, app.onError);
    },
    log: function(strData, dir) {
        if (app.logfile !== null) {
            app.logfile.createWriter(function(fileWriter) {
                try {
                    fileWriter.seek(fileWriter.length)
                }
                catch (e) {
                    console.log("file doesn't exist!");
                }
                fileWriter.write(dir + strData + "\n");
            });
        }
    },
    onData: function(data) { // data received from Display
        console.log(data);
        var strData = arrayToString(data)
        resultDiv.innerHTML = resultDiv.innerHTML + "Recv: " + strData + "<br/>";
        resultDiv.scrollTop = resultDiv.scrollHeight;
        app.log(strData, "> ")

        if (app.tcpClientSocketId != null) {
            // Navdy response to "AT Z" has been customised, lets replace it with
            // common ELM327 identifier as many apps need to see this.
            if (strData.trim() == "Navdy OBD-II default_on_v3\r\r>") {
                data = toArrayBuffer("\rELM327 v2.1\r>")
            }
            chrome.sockets.tcp.send(app.tcpClientSocketId, data,
                function(resultCode) {
                // console.log("Data sent to new TCP client connection.")
            });
        }
    },
    sendBtn: function(event) {
        var data = messageInput.value + "\r";
        app.sendData(data);
    },
    sendData: function(data) { // send data to Display
        data = toArrayBuffer(data);
        var strData = arrayToString(data)
        var success = function() {
            console.log("success");
            resultDiv.innerHTML = resultDiv.innerHTML + "Sent: " + strData + "<br/>";
            resultDiv.scrollTop = resultDiv.scrollHeight;
        };

        var failure = function() {
            alert("Failed writing data to Navdy");
        };

        bluetoothSerial.write(data, success, failure);
        app.log(strData, "< ")
    },
    disconnect: function(event) {
        bluetoothSerial.disconnect(app.showMainPage, app.onError);

        if (app.tcpServerSocketId != null) {
            chrome.sockets.tcpServer.disconnect(app.tcpClientSocketId);
            // app.tcpServerSocketId = null;
            app.tcpClientSocketId = null;
        }
        cordova.plugins.backgroundMode.disable();
        app.logfile.abort();
        app.logfile = null;

    },
    showMainPage: function() {
        mainPage.style.display = "";
        detailPage.style.display = "none";
    },
    showDetailPage: function() {
        mainPage.style.display = "none";
        detailPage.style.display = "";
    },
    setStatus: function(message) {
        console.log(message);

        window.clearTimeout(app.statusTimeout);
        statusDiv.innerHTML = message;
        statusDiv.className = 'fadein';

        // automatically clear the status with a timer
        app.statusTimeout = setTimeout(function () {
            statusDiv.className = 'fadeout';
        }, 5000);
    },
    onError: function(reason) {
        alert("ERROR: " + reason); // real apps should use notification.alert
    }
};
