# Navdy OBD

This mobile application opens a direct connection to the the OBD interface in the Navdy HUD (STN1110)

It requires the Navdy display to be running a rom with: 
* Hud: https://gitlab.com/alelec/navdy/navdy-display-Hud-java/commit/74fd5864753e56eab3ffa9f26a4cdc94c8a4cb01
* Obd: https://gitlab.com/alelec/navdy/navdy-display-Obd-java/commit/5cc4e8262bc5fe8091966c66dfa296a6465abb5a

This code requires [cordova-cli](https://github.com/apache/cordova-cli), which require [node.js](http://nodejs.org)

    $ npm install cordova -g

## Android
This code requires an Android device since the emulator does not support Bluetooth. Open you navdy app and ensure it's connected to your navdy display.

Build and deploy to an Android device. (Emulate deploys to the connected device.)

    $ cordova platform add android
    $ cordova run android -d

## iOS
The code requires an iOS device, rather than the emulator, since the emulator doesn't support Bluetooth.

Build the code

    $ cordova platform add ios
    $ cordova build ios

Open Xcode and deploy to your device

    $ open platforms/ios/Chat.xcodeproj
