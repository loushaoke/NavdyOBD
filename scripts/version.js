#!/usr/bin/env node

// Save hook under `project-root/hooks/before_prepare/`
//

var fs = require('fs');
const gitDescribeSync = require('git-describe').gitDescribeSync;

// Read config.xml
fs.readFile('config.xml', 'utf8', function(err, data) {
  if(err) {
    return console.log(err);
  }
  
  var vers = gitDescribeSync(".", {
      longSemver: true,
      customArguments: ["--tags"]})

  vers = vers.toString().replace(/^v/,'')
  
  data = data.replace(/(\<widget id="net\.alelec\.navdyobd" version=")[\d\.]+(" xmlns)/, "$1" + vers + "$2")
  
  // Write config.xml
  fs.writeFile('config.xml', data, function(err) {
    if(err) {
      return console.log(err);
    }  
    console.log(vers);
  });

});